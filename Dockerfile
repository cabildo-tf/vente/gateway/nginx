ARG NGINX_IMAGE_TAG=1.21.3-alpine

FROM nginx:${NGINX_IMAGE_TAG}

LABEL maintainer="ilorgar@gesplan.es"

COPY rootfs /

RUN apk add --no-cache \
	openssl=~1.1

EXPOSE 443

HEALTHCHECK --interval=30s --timeout=15s --start-period=1m --retries=10 \
	CMD wget --spider --no-check-certificate  -q https://localhost/nginx-health \
		|| (count=$(ps aux | grep openssl | wc -l); [ ${count} -gt 1 ]) \
		|| exit 1

CMD ["sh", "-c", "/entrypoint.sh"]
